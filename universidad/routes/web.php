<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('');
});

 Route::get('/alumno','Alumno@index');/*vista de registro de alumnos*/

Route::get('/nuevo', function () {
    return view('formulario');/*vista de formulario*/
});

Route::get('/alumno/eliminar/{id}','Alumno@eliminar')->where('id_alumno','[0-9]+')->name('eliminar');