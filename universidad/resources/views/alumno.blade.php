<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body class="container">
	<center>
	<a href="nuevo"><button class="btn btn-info">Nuevo</button></a>

	<table class="table	table-olver">
		 <thead class="thead-dark">
			<tr>
				<th>Nombre</th>
				<th>Apellido</th>
				<th>Edad</th>
				<th>Sexo</th>
				<th>Direccion</th>
				<th colspan="2"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($datos as $d)
			<tr>
				<td><?= $d->nombre; ?></td>
				<td><?= $d->apellido; ?></td>
				<td><?= $d->edad; ?></td>
				<td><?= $d->nombre_sexo; ?></td>
				<td><?= $d->direccion; ?></td>
				<td><a href="{{Route('eliminar',$d->id_alumno)}}"><button class="btn btn-danger">Eliminar</button></a></td>
				
			</tr>
			@endforeach
		</tbody>
		
	</table>
</center>
</body>
</html>