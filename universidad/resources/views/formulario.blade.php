<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
	<center>
		<br>
	<div class="col-sm-3 my-1">
		<label class="sr-only" for="inlineFormInputName">Nombre</label>
		<input type="text" name="nombre"class="form-control" style="border-color: #00A0B6" placeholder="Nombre">
	</div>
	<br>
    <div class="col-sm-3 my-1">
		<label class="sr-only" for="inlineFormInputName">Apellido</label>
		<input type="text" name="apellido" class="form-control" style="border-color: #00A0B6" placeholder="Apellido">
	</div>
	<br>
	<div class="col-sm-3 my-1">
		<label class="sr-only" for="inlineFormInputName">Edad</label>
		<input type="text" name="edad" class="form-control" style="border-color: #00A0B6" placeholder="Edad">
	</div>
	<div>
		<select name="sexo">
			<option value="">seleccione sexo</option>
			@foreach($sexo as $row)
			<option value="{{$row->id_sexo}}">{{$row->nombre_sexo}}</option>
			@endforeach
		</select>
	</div>

</center>
</body>
</html>