<?php

namespace App\Http\Controllers;
use App\estudiante;
use App\Procedure\DatosProcedure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Alumno extends Controller{
       public function __construct(DatosProcedure $DatosProcedure){
    	$this->DatosProcedure = $DatosProcedure;
    }
    public function index(){
    $datos = $this->DatosProcedure->getDatos();
    return view('alumno',compact('datos'));

    }
    public function eliminar($id){
    	DB::table('alumno')->where('id_alumno','=',$id)->delete();
    	return redirect('alumno');
    }
    public function GetSexo(){
    	$sexo =$this->DatosProcedure->getSexo();
    	return view('alumno',compact('sexo'));
    }


}
